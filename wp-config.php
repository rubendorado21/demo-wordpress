<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demo-wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7%wwo{&c8Q)UZ`^K7_#zTB5^JPaM>weM%<U@mOSY=HZO ZbTSJ1;;/.sZ,+r/#|p' );
define( 'SECURE_AUTH_KEY',  'r^|Vv9*cu)7/m;(U,GENBg/Fjb1>4ev`NG_t^I[jZwupA4v5[ O/fhVEqu!;@JB^' );
define( 'LOGGED_IN_KEY',    '_58_72Nad(h//BHd%nA@34PsXAj.U1Lf2[I[`OPbO*4&ywKkW#v&a|x )%`([Xh^' );
define( 'NONCE_KEY',        'N&~w_$%LEN0i3.n_w!W`/X_U}$Xfbo+A>sx{N$]BkG`HM^.2J#V04eZ *}s[IH67' );
define( 'AUTH_SALT',        'T<DF3Shl5k.sRQXv!,n{tpxOH;!rIU+usD}QRkLfrW{#{_2T vEgN2}j7-nY48A!' );
define( 'SECURE_AUTH_SALT', 'F sMe7LSfMk<H)1^U<dW[Z5A<nO@t)%?8[Df|OZ19J #zEa;2?KhexW*B)5RzN-+' );
define( 'LOGGED_IN_SALT',   '`*l&VJ.vMZ6;x}Or|xXtiPZt9;(mMhn9xCz1i3ZL_zL=11r3ChHPALz~#XzFRjz|' );
define( 'NONCE_SALT',       'tX|P-/SMEN))b>!zyrseF8b&%%?ga0(haT49,^<`|KGnK3E:OlN6QdM6V^o] MeW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
